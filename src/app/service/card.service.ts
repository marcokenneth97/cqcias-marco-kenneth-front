import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonaModel } from '../model/persona-model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private httpClient: HttpClient) {


  }

  getPersona(): Observable<PersonaModel[]> {
    return this.httpClient.get<PersonaModel[]>('http://localhost:9000/api/v1/persona' + '/list').pipe(map(res => res));
  }

  getPersonaId(id: number): Observable<PersonaModel[]> {
    return this.httpClient.get<PersonaModel[]>('http://localhost:9000/api/v1/persona' + '/listId/' + id).pipe(map(res => res));
  }

  savePersona(request: any): Observable<any> {
    return this.httpClient.post<any>('http://localhost:9000/api/v1/persona' + '/save', request).pipe(map(res => res));
  }

  updatePersona(request: any): Observable<any> {
    return this.httpClient.post<any>('http://localhost:9000/api/v1/persona' + '/update', request).pipe(map(res => res));
  }

  deletePersona(id: number): Observable<any> {
    return this.httpClient.get<any>('http://localhost:9000/api/v1/persona' + '/delete/' + id).pipe(map(res => res));
  }
}
