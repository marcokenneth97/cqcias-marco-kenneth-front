import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { PersonaModel } from 'src/app/model/persona-model';
import { CardService } from 'src/app/service/card.service';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})
export class CardComponent implements OnInit {

  listPersona: PersonaModel[] = [];
  formPersona: FormGroup = new FormGroup({});
  isUpdate: boolean = false;

  constructor(private cardService: CardService) { }

  dtopcion: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  //Inicio de variables y metodos de la vista
  ngOnInit(): void {
    this.dtopcion = {
      pagingType: 'full_numbers',
      paging: false,
      retrieve: true,
      language: {
        url: '//cdn.datatables.net/plug-ins/1.13.5/i18n/es-ES.json',
      }
    }
    this.list();
    this.formPersona = new FormGroup({
      id: new FormControl(''),
      nombre: new FormControl('', [Validators.required]),
      primer_apellido: new FormControl('', [Validators.required]),
      segundo_apellido: new FormControl('', [Validators.required]),
      telefono: new FormControl(''),
      estatus: new FormControl('1'),
      fecha_ins: new FormControl(''),
      fecha_upd: new FormControl('')
    });
  }

  //Metodo que carga los datos de DB
  list() {
    this.cardService.getPersona().subscribe(resp => {
      if (resp) {
        this.listPersona = resp;
        this.dtTrigger.next(null);
      }
    });
  }

  //Metodo que envia los datos y valida datos
  save() {
    this.formPersona.markAllAsTouched();
    if (this.formPersona.valid) {
      this.formPersona.controls['estatus'].setValue('1');
      this.formPersona.controls['fecha_ins'].setValue(new Date);
      this.formPersona.controls['fecha_upd'].setValue(new Date);
      this.cardService.savePersona(this.formPersona.value).subscribe(resp => {
        if (resp) {
          this.list();
        }
      });
    }
  }

  //Metodo que actualiza y valida campos
  update() {
    this.formPersona.markAllAsTouched();
    if (this.formPersona.valid) {
      this.formPersona.controls['estatus'].setValue('1');
      this.formPersona.controls['fecha_upd'].setValue(new Date);
      this.cardService.updatePersona(this.formPersona.value).subscribe(resp => {
        if (resp) {
          this.list();
          this.formPersona.reset();
        }
      });
    }
  }

  //Metodo que borra los datos de DB
  delete(id: any) {
    this.formPersona.controls['fecha_upd'].setValue(new Date);
    this.cardService.deletePersona(id).subscribe(resp => {
      if (resp) {
        this.list();
      }
    });
  }

  //Metodo que trae datos de un id
  listId(id: any) {
    this.cardService.getPersonaId(id).subscribe(resp => {
      if (resp) {
        this.listPersona = resp;
      }
    });
  }

  //Recetea el form
  newPersona() {
    this.isUpdate = false;
    this.formPersona.reset();
  }

  //Muestra datos de un registros seleccionado
  selectItem(item: any) {
    this.isUpdate = true;
    this.formPersona.controls['id'].setValue(item.id);
    this.formPersona.controls['nombre'].setValue(item.nombre);
    this.formPersona.controls['primer_apellido'].setValue(item.primer_apellido);
    this.formPersona.controls['segundo_apellido'].setValue(item.segundo_apellido);
    this.formPersona.controls['telefono'].setValue(item.telefono);
    this.formPersona.controls['estatus'].setValue(item.estatus);
    this.formPersona.controls['fecha_ins'].setValue(item.fecha_ins);
    this.formPersona.controls['fecha_upd'].setValue(item.fecha_upd);
  }

  //Validacion de datos en input
  get nombre() { return this.formPersona.get('nombre'); }
  get primer_apellido() { return this.formPersona.get('primer_apellido'); }
  get segundo_apellido() { return this.formPersona.get('segundo_apellido'); }
}
